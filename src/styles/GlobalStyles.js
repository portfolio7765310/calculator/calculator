import { StyleSheet } from 'react-native';
import { myColors } from './Colors';

export const Styles = StyleSheet.create({
	btnBlue: {
		width: 72,
		height: 72,
		borderRadius: 10,
		backgroundColor: myColors.blue,
		justifyContent: 'center',
		alignItems: 'center',
		margin: 1
	},
	btnDark: {
		width: 72,
		height: 72,
		borderRadius: 10,
		backgroundColor: myColors.btnDark,
		justifyContent: 'center',
		alignItems: 'center',
		margin: 1
	},
	btnLight: {
		width: 72,
		height: 72,
		borderRadius: 10,
		backgroundColor: myColors.white,
		justifyContent: 'center',
		alignItems: 'center',
		margin: 1
	},
	btnGray: {
		width: 72,
		height: 72,
		borderRadius: 10,
		backgroundColor: myColors.btnGray,
		justifyContent: 'center',
		alignItems: 'center',
		margin: 1
	},
	smallTextLight:{
		fontSize: 32,
		color: myColors.white
	},
	smallTextDark:{
		fontSize: 32,
		color: myColors.black
	},

	row: {
		maxWidth: '100%',
		flexDirection: 'row'
	},
	viewBottom: {
		position: 'fixed',
		bottom: 50
	},
	screenFirstNumber: {
		fontSize: 96,
		color: myColors.gray,
		fontWeight: '200',
		alignSelf: 'flex-end'
	},
	screenFirstNumber: {
		fontSize: 40,
		color: myColors.gray,
		fontWeight: '200',
		alignSelf: 'flex-end'
	}
});