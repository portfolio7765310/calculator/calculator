import * as React from 'react';
import Button from './Button';
import { View, Text } from 'react-native';
import { Styles } from '../styles/GlobalStyles';
import { myColors } from '../styles/Colors';
import History from './History';
import Icon from '@mdi/react';
import { mdiHistory } from '@mdi/js';


export default function MyKeyboard(){
	const [firstNumber, setFirstNumber] = React.useState('');
	const [secondNumber, setSecondNumber] = React.useState('');
	const [operation, setOperation] = React.useState('');
	const [result, setResult] = React.useState(null);
	const [history, setHistory] = React.useState([]);
	const [showComponent, setShowComponent] = React.useState(false);

	/*	
		const handleShowComponent = () => {
				// setShowComponent(!showComponent);
				navigation.navigate('History', { paramKey: `${mdiHistory}` })
			}
	*/

	const handleNumberPress = (buttonValue: string) => {
		if(firstNumber.length <10){
			setFirstNumber(firstNumber + buttonValue);
		}
	};

	const handleOperationPress = (buttonValue: string) => {
		if(operation !== ''){
		    getResult();
		    setOperation(buttonValue);
		    setSecondNumber(result.toString());
		    setFirstNumber('');
		    setResult(null);
		} 
		else{
		    setOperation(buttonValue);
		    setSecondNumber(firstNumber);
		    setFirstNumber('');
		}
	};


	const clear = () => {
		setFirstNumber('');
		setSecondNumber('');
		setOperation('');
		setResult(null);
	};

	const getResult = () => {
	  	/*if (!firstNumber || !operation) {
	    	return;
	  	}*/
	  	let previousResult;
	  		switch(operation){
	    		case "+":
			      	previousResult = parseInt(secondNumber) + parseInt(firstNumber);
			      	break;
			    case "-":
			      	previousResult = parseInt(secondNumber) - parseInt(firstNumber);
			      	break;
			    case "*":
			     	previousResult = parseInt(secondNumber) * parseInt(firstNumber);
			      	break;
			    case `\u00F7`:
			      	previousResult = parseInt(secondNumber) / parseInt(firstNumber);
			      	break;
			    case "%":
			      	previousResult = parseInt(secondNumber) % parseInt(firstNumber);
			      	break;
			    default:
			      	previousResult = 0;
			      	break;
		}

		const newOperation = `${secondNumber} ${operation} ${firstNumber} = ${previousResult}`;
		setHistory([...history, newOperation]);
		setResult(previousResult);
		console.log(history);
	};


	const displayNumbers = (buttonValue: string) => {
		if(result !== null){
			return <Text style={result < 99999 ? [Styles.screenFirstNumber, {fontSize: 70, color: myColors.result}] : [Styles.screenFirstNumber, {fontSize: 70, color: myColors.result}]}>{result.toString()}</Text>;
		}
		if(firstNumber && firstNumber.length < 6){
			return <Text style={[Styles.screenFirstNumber, {fontSize: 70, color: 'green'}]}>{firstNumber}</Text>;
		}
		if(firstNumber === ''){
			return <Text style={[Styles.screenFirstNumber, {fontSize: 70}]}>{'0'}</Text>;
		}
		if(firstNumber.length > 5){
			return (<Text style={[Styles.screenFirstNumber, {fontSize: 50}]}>{firstNumber}</Text>);
		}
		if(firstNumber.length > 7){
			return (<Text style={[Styles.screenFirstNumber, {fontSize: 50}]}>{firstNumber}</Text>);
		}
		if(buttonValue === `\u21BA`){
			return (<Text style={[Styles.screenFirstNumber, {fontSize: 50}]}></Text>);
		}
	}

	return(
		<View style={Styles.viewBottom}>
			<View 
				style={{
					height: 120,
					width: "100%",
					justifyContent: "flex-end",
					alignSelf: "center",

				}}
			>
				<Text style={[Styles.screenSecondNumber, {fontSize: 40, color: 'orange'}]}>
				{secondNumber}
					<Text style={{color: 'orange', fontSize:40, fontWeight: '500'}}>{operation}</Text>
					<Text style={[Styles.screenFirstNumber, {fontSize: 40, color: 'orange'}]}>
					{firstNumber}</Text>
				</Text>
				{displayNumbers()}
			</View>
			
			<View style={Styles.row}>
				<Button title="AC" isGray onPress={clear} />
				<Button title="⌫" isGray onPress={() => setFirstNumber(firstNumber.slice(0, -1))} />
				<Button title="%" isGray onPress={() => handleOperationPress("%")} />
				<Button title="÷" isBlue onPress={() => handleOperationPress(`\u00F7`)} />
			</View>
			<View style={Styles.row}>
				<Button title="7" onPress={() => handleNumberPress("7")} />
				<Button title="8" onPress={() => handleNumberPress("8")} />
				<Button title="9" onPress={() => handleNumberPress("9")} />
				<Button title="x" isBlue onPress={() => handleOperationPress("*")} />
			</View>
			<View style={Styles.row}>
				<Button title="4" onPress={() => handleNumberPress("4")} />
				<Button title="5" onPress={() => handleNumberPress("5")} />
				<Button title="6" onPress={() => handleNumberPress("6")} />
				<Button title="-" isBlue onPress={() => handleOperationPress("-")} />
			</View>
			<View style={Styles.row}>
				<Button title="1" onPress={() => handleNumberPress("1")} />
				<Button title="2" onPress={() => handleNumberPress("2")} />
				<Button title="3" onPress={() => handleNumberPress("3")} />
				<Button title="+" isBlue onPress={() => handleOperationPress("+")} />
			</View>
			<View style={Styles.row}>
				<Button title="." onPress={() => handleNumberPress(".")} />
				<Button title="0" onPress={() => handleNumberPress("0")} />
				<Button title={<Icon path={mdiHistory} size={2} />} onPress={() => clear()} />
				<Button title="=" isBlue onPress={() => getResult()} />
			</View>
		</View>

	)

}