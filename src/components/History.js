import React from 'react';
import { View, Text } from 'react-native';
import { myColors } from '../styles/Colors';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import MyKeyboard from './MyKeyboard';

export default function History({ history }) {
  return (
    <View style={{ flex: 1, marginVertical: 10 }}>
      <Text style={{ color: myColors.result, fontSize: 20, fontWeight: 'bold', marginBottom: 5 }}>History:</Text>
      {Array.isArray(history) && history.map((item, index) => (
        <Text key={index} style={{ color: myColors.text, fontSize: 16 }}>{item}</Text>
      ))}
    </View>
  );
};
