import { useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, SafeAreaView, Switch } from 'react-native';
import { ThemeContext } from './src/context/ThemeContext';
import { myColors } from './src/styles/Colors';
import Button from './src/components/Button';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import MyKeyboard from './src/components/MyKeyboard';
import History from './src/components/History';



export default function App() {
  const Stack = createStackNavigator();
  const [theme, setTheme] = useState('light');

  return (
    
    <ThemeContext.Provider value={theme} >
    <SafeAreaView style={theme === 'light' ? styles.container: [styles.container, {backgroundColor: '#F1F2F3'}]}>
      <StatusBar style="auto" />
      <Switch
      value={theme === 'light'}
      onValueChange={() => setTheme(theme === 'light' ? 'dark' : 'light')}
      />
        <MyKeyboard />
    </SafeAreaView>
    </ThemeContext.Provider>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: myColors.dark,
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
});
